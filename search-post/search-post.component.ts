import { Component } from '@angular/core';
import { SearchEngineComponent } from '../../components/search-engine/search-engine.component';
import { ToolbarComponent } from '../../components/toolbar/toolbar.component';
import { ApartmentsHighlightedComponent } from '../../components/apartments-highlighted/apartments-highlighted.component';

@Component({
  selector: 'app-search-post',
  standalone: true,
  imports: [SearchEngineComponent,ToolbarComponent,ApartmentsHighlightedComponent],
  templateUrl: './search-post.component.html',
  styleUrl: './search-post.component.scss'
})
export class SearchPostComponent {

}
