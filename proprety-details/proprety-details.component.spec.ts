import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropretyDetailsComponent } from './proprety-details.component';

describe('PropretyDetailsComponent', () => {
  let component: PropretyDetailsComponent;
  let fixture: ComponentFixture<PropretyDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PropretyDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PropretyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
