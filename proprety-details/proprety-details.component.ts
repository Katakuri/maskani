import { Component } from '@angular/core';
import { PropretyIndividualDetailsComponent } from '../../components/proprety-individual-details/proprety-individual-details.component';

@Component({
  selector: 'app-proprety-details',
  standalone: true,
  imports: [PropretyIndividualDetailsComponent],
  templateUrl: './proprety-details.component.html',
  styleUrl: './proprety-details.component.scss'
})
export class PropretyDetailsComponent {
}
