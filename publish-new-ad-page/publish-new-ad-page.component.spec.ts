import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishNewAdPageComponent } from './publish-new-ad-page.component';

describe('PublishNewAdPageComponent', () => {
  let component: PublishNewAdPageComponent;
  let fixture: ComponentFixture<PublishNewAdPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PublishNewAdPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PublishNewAdPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
