import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListApartmentsPageComponent } from './list-apartments-page.component';

describe('ListApartmentsPageComponent', () => {
  let component: ListApartmentsPageComponent;
  let fixture: ComponentFixture<ListApartmentsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListApartmentsPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListApartmentsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
