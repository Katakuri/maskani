import { Component } from '@angular/core';
import {SignUpComponent} from '../../components/signup/signup.component';

@Component({
  selector: 'app-signup-page',
  standalone: true,
  imports: [SignUpComponent],
  templateUrl: './signup-page.component.html',
  styleUrl: './signup-page.component.scss'
})
export class SignupPageComponent {

}
