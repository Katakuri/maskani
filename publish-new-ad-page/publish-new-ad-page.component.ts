import { Component } from '@angular/core';
import { PublishNewAdComponent } from '../../components/publish-new-ad/publish-new-ad.component';

@Component({
  selector: 'app-publish-new-ad-page',
  standalone: true,
  imports: [PublishNewAdComponent],
  templateUrl: './publish-new-ad-page.component.html',
  styleUrl: './publish-new-ad-page.component.scss'
})
export class PublishNewAdPageComponent {

}
