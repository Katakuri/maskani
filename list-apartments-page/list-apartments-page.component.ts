import { Component } from '@angular/core';
import { ListApartmentsComponent } from '../../components/list-apartments/list-apartments.component';
@Component({
  selector: 'app-list-apartments-page',
  standalone: true,
  imports: [ListApartmentsComponent],
  templateUrl: './list-apartments-page.component.html',
  styleUrl: './list-apartments-page.component.scss'
})
export class ListApartmentsPageComponent {

}
